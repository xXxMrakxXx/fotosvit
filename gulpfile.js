var cfg = {
    node: 'node_modules/',
    dev:  '',
    paths: {    //  Common paths
        vendor: 'vendor/',
        fonts: 'fonts/',
        img: 'images/',
        css: 'css/',
        less: 'less/'
    }
};

//  Dev pathes
var vendor = cfg.dev + cfg.paths.vendor,  //  Third libraries
    fonts = cfg.dev + cfg.paths.fonts,
    images = cfg.dev + cfg.paths.img;

/**
 *  main page
 */
var alkima = {
    dev: {
        css: cfg.paths.css,
        less: cfg.paths.less
    },
    prod: {
        css: cfg.prod  //  Fake for future
    }
};

// browser-sync watched files
// automatically reloads the page when files changed
var browserSyncWatchFiles = [
    './css/*.min.css',
    './js/*.min.js',
    './*.html'
];
// browser-sync options
var browserSyncOptions = {
    server: "./"
};

var gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    del = require( 'del' ),
    less = require('gulp-less'),
    sourcemaps = require( 'gulp-sourcemaps' ),
    plumber = require( 'gulp-plumber' ),
    cleanCSS = require('gulp-clean-css'),
    rename = require( 'gulp-rename' ),
    autoprefixer = require( 'gulp-autoprefixer' ),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create();


function del_folder( folder_name ) {
    del.sync( folder_name );
}
del_folder.description = 'Delete folder';


/**
 *  Build & minify theme CSS
 */
function lessCompile(less_folder, dest_folder) {
    return gulp.src(less_folder)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer({browsers: ['last 10 versions', '> 1%', 'ie 9', 'ie 10'], cascade: true }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(dest_folder));
}


gulp.task( 'less:main', function(done){
    gulp.task(lessCompile( cfg.paths.less + 'main.less', cfg.paths.css ));
    done();
});
gulp.task( 'less:critical', function(done){
    gulp.task(lessCompile( cfg.paths.less + 'critical.less', cfg.paths.css ));
    done();
});

gulp.task( 'css:main',
    gulp.series(
        gulp.parallel(
            'less:main',
            'less:critical'
        )
    )
);

/**
 * Copy third party libraries from /node_modules into /vendor
 */
gulp.task( 'vendor:del', function(done) {
    del_folder( vendor );
    done();
});
// jQuery
gulp.task( 'vendor:jquery', function(done) {
    gulp.src( cfg.node + 'jquery/dist/jquery.min.*' )
        .pipe( gulp.dest( vendor + 'jquery' ));
    done();
});
// jQuery Easing
gulp.task( 'vendor:jqueryeasing', function(done) {
    gulp.src( cfg.node + 'jquery.easing/jquery.easing.min.js' )
        .pipe( gulp.dest( vendor + 'jquery-easing' ));
    done();
});
// Bootstrap + popper
gulp.task( 'vendor:bootstrap', function(done) {
    var path_tmp = cfg.node + 'bootstrap/dist/';
    gulp.src([
        path_tmp + '**/*.min.*',
        '!' + path_tmp + 'js/bootstrap.min.*',
        '!' + path_tmp + 'css/bootstrap-grid*',
        '!' + path_tmp + 'css/bootstrap-reboot*'
    ])
        .pipe( gulp.dest( vendor + 'bootstrap' ));
    done();
});
// Font Awesome 5
gulp.task( 'vendor:fontawesome', function(done) {
    path_tmp = cfg.node + '@fortawesome/fontawesome-free/**/';
    gulp.src([
        path_tmp + 'all.min.css',
        path_tmp + 'webfonts/*'
    ])
        .pipe( gulp.dest( vendor + 'fontawesome' ));
    done();
});
// Slick
gulp.task( 'vendor:slick', function(done) {
    gulp.src( cfg.node + 'slick-carousel/slick/**/*.*' )
        .pipe( gulp.dest( vendor + 'slick' ));
    done();
});
gulp.task( 'vendor',
    gulp.series(
        'vendor:del',
        gulp.parallel(
            'vendor:jquery',
            'vendor:jqueryeasing',
            'vendor:bootstrap',
            'vendor:fontawesome',
            'vendor:slick'
        )
    )
);

/**
 *  Transfer from node_modules to theme & rebuild theme css
 */
gulp.task( 'default',
    gulp.parallel(
        'vendor',
        'css:main'
    ));


/**
 * * Build task to minify css and js
 */
function cssCompile(css_folder, dest_folder) {
    return gulp.src(css_folder)
        .pipe(cleanCSS())
        .pipe(rename({suffix: ".min"}))
        .pipe(purge())
        .pipe(gulp.dest(dest_folder));
}

gulp.task('css-min', function(done){
    gulp.task(cssCompile([cfg.paths.css + '*.css','!' + cfg.paths.css + '*.min.css'], cfg.paths.css));
    done();
});

function scriptsCompile() {
    return (
        gulp
            .src(["vendor/**/*"])
            .pipe(plumber())
            .pipe(uglify())
            .pipe(rename({suffix: ".min"}))
            .pipe(gulp.dest("js/"))
    );
}
gulp.task("js-min", gulp.series(scriptsCompile));

gulp.task('build', gulp.parallel('css-min', 'js-min'));

gulp.task( 'watch', function() {
    gulp.watch([
        cfg.paths.less + '**/*.less'
    ], gulp.series( 'css:main' ));
});

/**
 *  PHP server + browser sync + watch
 */
gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
    gulp.watch("./css/*.css").on('change', browserSync.reload);
    gulp.watch("./*.html").on('change', browserSync.reload);
});

gulp.task( 'connect-sync',
    gulp.series(
        'browser-sync',
        'watch'
    )
);