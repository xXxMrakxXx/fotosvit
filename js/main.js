$(document).ready(function () {
    // init slick slider
    $('.slider__portfolio').slick({
        centerMode: true,
        arrows: false,
        dots: true,
        infinite: true,
        draggable: false,
        focusOnSelect: true,
        centerPadding: '0',
        slidesToShow: 5,
        lazyLoad: 'ondemand',
        autoplay: true,
        autoplaySpeed: 5000,
        /*responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]*/
    });
    // init slick slider
    $('.slider__reviews').slick({
        centerMode: true,
        arrows: false,
        dots: true,
        infinite: true,
        focusOnSelect: true,
        centerPadding: '0',
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 5000
    });

    // nav anchor
    $('.nav__header li').on('click', 'a', function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top - 40;

        $('body, html').animate({scrollTop: top}, 1500);
    });

    // mobile menu button
    $('.nav__header-btn').click(function () {
       $('.nav__header-btn').toggleClass('active');
       $('.nav__header').toggle('slow').toggleClass('active');
    });

    //fixed menu
    var menuHeight = $('.header').innerHeight();

    if($(window).scrollTop() > menuHeight){
        $('.header').addClass('header-fixed');
    }
    $(window).scroll(function(){
        if($(this).scrollTop() > menuHeight){
            $('.header').addClass('header-fixed');
        }
        else if ($(this).scrollTop() < menuHeight){
            $('.header').removeClass('header-fixed');
        }
    });

});

//youtube load image, on click change on iframe
( function() {

    var youtube = document.querySelectorAll( ".youtube" );

    for (var i = 0; i < youtube.length; i++) {

        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";

        var image = new Image();
        image.src = source;
        image.addEventListener( "load", function() {
            youtube[ i ].appendChild( image );
        }( i ) );

        youtube[i].addEventListener( "click", function() {

            var iframe = document.createElement( "iframe" );

            iframe.setAttribute( "frameborder", "0" );
            iframe.setAttribute( "allowfullscreen", "" );
            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

            this.innerHTML = "";
            this.appendChild( iframe );
        } );
    };

} )();